// nopepad.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#define DH(c) printf("%d\n", c)

BOOL CALLBACK handle_callback(HWND h, LPARAM i)
{
	TCHAR buff[1024] = {0};
	int len;

	if ((len = SendMessage(h,WM_GETTEXT,1024,(LPARAM)buff)))
	{
		for (int i=0;i<len;i++)
		{
			if (isprint(buff[i]))
				printf("%c", (unsigned char)buff[i]);
		}

		return FALSE;
	}
	return TRUE;
}

int _tmain(int argc, _TCHAR* argv[])
{
	char c;
	printf("Press Enter to dump current content of notepad or 'x' to exit\n");

	do
	{
		c = getchar();
		HWND handle = FindWindow(TEXT("notepad"),NULL);
		EnumChildWindows(handle, handle_callback, NULL);
	} while (c != 'x');

	return 0;
}

